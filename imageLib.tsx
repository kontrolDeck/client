
/**
 * This class can request and stores the images coming from a websocket server
 *
 * @class      ImageLib (name)
 */
export default class ImageLib {
    static instance = ImageLib.instance || new ImageLib();
    static ws;
    static imageLib;

    /**
     * Sets the web socket.
     *
     * @param      {JS_Qt_Websocket}  websocket  The websocket
     */
    setWebSocket(websocket)
    {
        this.ws = websocket;
        this.imageLib = [];
    }

    /**
     * Called when the websocket answered one of our requests
     *
     * @param      {string}         imagePath  The image path (used as the image ID)
     * @param      {base64 string}  data       The content of the image in based64
     */
    receivedImage(imagePath, data)
    {
        this.imageLib[imagePath] = data;
    }

    /**
     * Gets an image. Returns the image in base 64 if it is available.
     * If it is not available it requests it on the websocket and returns null.
     *
     * @param      {string}         imagePath  The image path (used as an ID)
     * @return     {(string|null)}  The image in base64 is available, null otherwise.
     */
    getImage(imagePath)
    {
        if(imagePath)
        {
            if(this.imageLib[imagePath] === "requested")
            {
                // Do not ask again for an image we are waiting to receive
            }
            else if(this.imageLib[imagePath])
            {
                return this.imageLib[imagePath];
            }
            else
            {
                this.ws.sendCommand("imgRequest", {"path": imagePath});
                this.imageLib[imagePath] = "requested";
                return null;
            }
        }
    }
}


