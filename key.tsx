import React from 'react';
import { StyleSheet, Text, View, Button, Alert, TouchableOpacity, Image } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import ImageLib from './imageLib';

/**
 * This class describes a key.
 *
 * @class      Key (name)
 */
export default class Key extends React.Component {

  state = {

  }

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount () {

  }

  /**
   * Callback when the user presses the key
   */
  handleClick() {

    // Send the trigger only if the button is not already busy
    if(this.props.busy == false)
    {
      this.props.onAction(this.props.id, "press");
    }
  }
  
  /**
   * Color manipulation function
   * for more informations: https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
   *
   * @param      {number}           p       Color shift amount (between 0 and 1)
   * @param      {color}            c0      The c 0
   * @param      {color}            c1      The c 1
   * @param      {<type>}           l       { parameter_description }
   * @return     {color}            The resulting color
   */
  pSBC(p,c0,c1,l){
      let r,g,b,P,f,t,h,i=parseInt,m=Math.round,a=typeof(c1)=="string";
      if(typeof(p)!="number"||p<-1||p>1||typeof(c0)!="string"||(c0[0]!='r'&&c0[0]!='#')||(c1&&!a))return null;
      if(!this.pSBCr)this.pSBCr=(d)=>{
          let n=d.length,x={};
          if(n>9){
              [r,g,b,a]=d=d.split(","),n=d.length;
              if(n<3||n>4)return null;
              x.r=i(r[3]=="a"?r.slice(5):r.slice(4)),x.g=i(g),x.b=i(b),x.a=a?parseFloat(a):-1
          }else{
              if(n==8||n==6||n<4)return null;
              if(n<6)d="#"+d[1]+d[1]+d[2]+d[2]+d[3]+d[3]+(n>4?d[4]+d[4]:"");
              d=i(d.slice(1),16);
              if(n==9||n==5)x.r=d>>24&255,x.g=d>>16&255,x.b=d>>8&255,x.a=m((d&255)/0.255)/1000;
              else x.r=d>>16,x.g=d>>8&255,x.b=d&255,x.a=-1
          }return x};
      h=c0.length>9,h=a?c1.length>9?true:c1=="c"?!h:false:h,f=this.pSBCr(c0),P=p<0,t=c1&&c1!="c"?this.pSBCr(c1):P?{r:0,g:0,b:0,a:-1}:{r:255,g:255,b:255,a:-1},p=P?p*-1:p,P=1-p;
      if(!f||!t)return null;
      if(l)r=m(P*f.r+p*t.r),g=m(P*f.g+p*t.g),b=m(P*f.b+p*t.b);
      else r=m((P*f.r**2+p*t.r**2)**0.5),g=m((P*f.g**2+p*t.g**2)**0.5),b=m((P*f.b**2+p*t.b**2)**0.5);
      a=f.a,t=t.a,f=a>=0||t>=0,a=f?a<0?t:t<0?a:a*P+t*p:0;
      if(h)return"rgb"+(f?"a(":"(")+r+","+g+","+b+(f?","+m(a*1000)/1000:"")+")";
      else return"#"+(4294967296+r*16777216+g*65536+b*256+(f?m(a*255):0)).toString(16).slice(1,f?undefined:-2)
  }

  render() {

    let color = this.props.color;

    if(this.props.busy == true)
    {
      // Will mix with transparent color
      // Works well with already transparent colors
      let transparent = "#ffffff00";
      color = this.pSBC(-0.4, color, transparent);
    }

    // Ask for a 20% darker color for the gradiant
    let darkColor = this.pSBC (-0.5, color);


    let content = (<Text style={{
            textAlign: 'center',
            fontSize: 20,
          }}>{this.props.title}</Text>);

    let iconImg = ImageLib.instance.getImage(this.props.icon);

    if(iconImg)
    {
      let url = "data:image/png;base64," + iconImg;
      let opacity = 1;

      if(this.props.busy == true)
      {
        opacity = 0.5;
      }

      content = (<Image source={{uri: url}} style={{
              height: "90%",
              width: "90%",
              resizeMode: 'contain',
              opacity: opacity}} />)
    }

    return (
      <View style={{
          position: "absolute",
          left: this.props.x+"%",
          top: this.props.y+"%",
          width: this.props.w+"%",
          height: this.props.h+"%",
        }}>
        <TouchableOpacity style={{
            height: "100%",
            width: "100%",
            backgroundColor: color,
            border: "1px solid black",
            borderRadius: 10,
          }}
          onPress={this.handleClick}
        >
          <LinearGradient
            colors={[darkColor, color]}
            start={[0, 0]}
            end={[1, 0]}
            style={{
              height: "100%",
              width: "100%",
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 10,
            }}>
            {content}
          </LinearGradient>
        </TouchableOpacity>
      </View>
	);
  }
}

const styles = StyleSheet.create({

});



