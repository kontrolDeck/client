import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, Button, Alert, Image, Vibration, AsyncStorage } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import Constants from 'expo-constants';
import { activateKeepAwake, deactivateKeepAwake } from 'expo-keep-awake';

import JS_Qt_Websocket from './js-qt-websocket/js/js_qt_websocket';

import Key from './key';
import ImageLib from './imageLib';


/**
 * This class describes a pannel.
 *
 * @class      Pannel (name)
 */
export default class Pannel extends React.Component {

  state = {
    ws: null,
    controls: [],
    hasPermission: false,
    serverAddress: "",
  }

  constructor(props) {
    super(props);
    this.handleAction = this.handleAction.bind(this);
    this.handleBarCodeScanned = this.handleBarCodeScanned.bind(this);

    this.getPermission();

    activateKeepAwake();
  }

  /**
   * Gets the permission to the camera for scanning qr codes
   *
   * @return     {Promise}  The permission.
   */
  async getPermission() {

    let status  = await BarCodeScanner.requestPermissionsAsync();
    this.setState({hasPermission: status.granted});
  }

  /**
   * Saves a websocket address to reload automaticaly next time.
   *
   * @param      {string}   address  The address
   * @return     {Promise}  The finishing of the save
   */
  async saveAddress(address) {

    try {
      await AsyncStorage.setItem('serverAddress', address);
      console.log("Saved the adress");
    } catch (error) {
        // Error saving data
    }
  }

  /**
   * Tries to reload a previously save websocket address
   *
   * @return     {Promise}  The fetching of the address
   */
  async reloadServerAddress() {

    try {
      const value = await AsyncStorage.getItem('serverAddress');

      if (value !== null) {
        // Our data is fetched successfully
        this.setState({serverAddress: value});
        this.handleWebSocketSetup(value);
        console.log("reloaded the adress");
      }
    } catch (error) {
        // Error retrieving data
    }
  }

  componentDidMount () {

    this.reloadServerAddress();
  }

  /**
   * Called to create the new websocket to use
   *
   * @param      {string}  address  The address
   */
  handleWebSocketSetup(address) {
    const ws = new JS_Qt_Websocket(address);
    ImageLib.instance.setWebSocket(ws);

    ws.onopen = () => {
      // connection opened
      this.setState({serverAddress: address});
      this.saveAddress(address);

      // Once connected ask for the pannel
      ws.sendCommand("refresh");
    };

    ws.onmessage = (e) => {

      var message = JSON.parse(e.data);

      if(message.command === "controller")
      {
        // Test if we have the same version numbers
        if(message.data.version != Constants.manifest.version)
        {
          alert("Please update your desktop application to version: " + Constants.manifest.version);
        }
        else
        {
          this.setState({controls: message.data.pannel.controls, background: message.data.pannel.background});
        }
      }
      else if(message.command === "image")
      {
        ImageLib.instance.receivedImage(message.data.path, message.data.binary);
        this.forceUpdate();
      }
    };

    ws.onerror = (e) => {
      // an error occurred
      //console.log("error");
      //console.log(e.code, e.reason);
      this.setState({serverAddress: ""});
    };

    ws.onclose = (e) => {
      // connection closed
      this.setState({controls: {}});
    };

    this.setState({ws});
  }

  /**
   * Called when a key is pressed
   *
   * @param      {string}  id      The identifier
   * @param      {string}  action  The action (can only be "press" for now)
   */
  handleAction(id, action) {

    const ws = this.state.ws;
    ws.sendCommand(action, {'id': id});
  }

  /**
   * When we successfully scanned a QR code
   *
   * @param      {json}  type    The type
   * @param      {json}  data    The data
   */
  handleBarCodeScanned(type, data)
  {
    Vibration.vibrate();
    this.handleWebSocketSetup(type.data);
  };

  render() {

    var controls = this.state.controls;

    const keys = []
    for(let i=0; i<controls.length; i++)
    {
      let currentKey = controls[i];

      // Translate the color code from "#ARGB" to "#RGBA"
      let color = currentKey.color.slice(3, 9);
      let alpha = currentKey.color.slice(1, 3);
      let colorCode = "#" + color + alpha;

      keys.push(<Key
                  key={i}
                  id={currentKey.id}
                  title={currentKey.title}
                  color={colorCode}
                  busy={currentKey.busy}
                  icon={currentKey.icon}
                  x={currentKey.pos.x}
                  y={currentKey.pos.y}
                  w={currentKey.size.w}
                  h={currentKey.size.h}
                  onAction={this.handleAction}
                />)
    }

    let maincontent;

    if(keys.length == 0)
    {
      if(this.state.hasPermission == false){
        maincontent = (<Text style={{
            textAlign: "center",
            color: "#ffffff",
          }}>Enable the camera.</Text>);
      }
      else
      {
        maincontent = (<BarCodeScanner
              onBarCodeScanned={this.handleBarCodeScanned}
              style={StyleSheet.absoluteFillObject}
            ><Text style={{
            textAlign: "center",
            color: "#ffffff",
            fontSize: 30,
            fontWeight: "bold",
          }}>Scan the QR code of the server app.</Text></BarCodeScanner>); 
      }
    }
    else
    {
      maincontent = keys;
    }

    let background;

    if(this.state.background)
    {
      let backgroundImg = ImageLib.instance.getImage(this.state.background);

      let backgroundUrl = "data:image/png;base64," + backgroundImg;

      background = (<Image source={{uri: backgroundUrl}} style={{
        height: "100%",
        width: "100%",
        resizeMode: "cover",
        opacity: 1,}} />)
    }

    return (
      <View style={styles.pannel}>
        {background}
        {maincontent}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  pannel: {
    backgroundColor: '#000',
    width: "100%",
    height: "100%"
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
});

