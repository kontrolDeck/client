import React from 'react';
import { StyleSheet, Text, View, Button, Alert, StatusBar } from 'react-native';
import Pannel from './pannel';


export default function App() {
  return (
    <View style={styles.container}>
      <StatusBar hidden />
      <Pannel/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
  },
});
